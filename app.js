const express = require('express')
const cors = require('cors')
const session = require('express-session')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')

require('./config/models')
const database = require('./config/database')
const router = require('./routers/index.router')

require('dotenv').config()
const app = express()
const port = process.env.PORT || 5001

app.disable('x-powered-by')
app.use(cors({ origin: true }))
app.use(helmet())

app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(cookieParser())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 86400000 }
}))
app.use('/', router)
app.all('*', (req, res) => { return res.status(404).render('404') })

app.listen(port, async () => {
    try {
        await database.authenticate()
        await database.sync({})
        console.log('Database connected...')
        console.log(`Server is running: http://localhost:${port}`)
    } catch (error) {
        throw error
    }
})