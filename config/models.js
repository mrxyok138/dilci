const { DataTypes } = require('sequelize')
const database = require('./database')

const Users = database.define("users", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    image: { type: DataTypes.STRING, defaultValue: 'user2.png' },
    username: { type: DataTypes.STRING, allowNull: false, unique: true },
    password: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING, allowNull: true, unique: true },
    phone: { type: DataTypes.STRING, allowNull: false, unique: true },
    isActive: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true }
})

const User_History = database.define("user_histories", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    point: { type: DataTypes.INTEGER, defaultValue: 0 }
})

const Languages = database.define("languages", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    name: { type: DataTypes.STRING, allowNull: false, unique: true },
    image: { type: DataTypes.STRING, allowNull: false }
})

const Roles = database.define("roles", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    name: { type: DataTypes.STRING, allowNull: false, unique: true }
})

const Words = database.define("words", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    word: { type: DataTypes.STRING, allowNull: false },
    transcription: { type: DataTypes.STRING, allowNull: false },
    point: { type: DataTypes.INTEGER, allowNull: false }
})

const TurkmenWords = database.define("turkmen_words", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    word: { type: DataTypes.STRING, allowNull: false }
})

const Levels = database.define("levels", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    name: { type: DataTypes.STRING, allowNull: false },
    point: { type: DataTypes.INTEGER, allowNull: false }
})

const Notifications = database.define("notifications", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    message: { type: DataTypes.TEXT, allowNull: false }
})

const Messages = database.define("messages", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    username: { type: DataTypes.STRING, allowNull: false },
    email: { type: DataTypes.STRING, allowNull: false, validate: { isEmail: true } },
    message: { type: DataTypes.TEXT, allowNull: false }
})

const Games = database.define("games", {
    id: { type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, unique: true },
    gameId: { type: DataTypes.INTEGER, allowNull: false },
    sectionId: { type: DataTypes.BIGINT, allowNull: false},
    status: { type: DataTypes.ENUM({ values: ['true', 'false', 'unanswered'] }), allowNull: false }
})

Users.hasMany(Games)
Games.belongsTo(Users)

Words.hasMany(Games)
Games.belongsTo(Words)

Roles.hasOne(Users)
Users.belongsTo(Roles)

Users.hasMany(User_History)
User_History.belongsTo(Users)

Languages.hasMany(User_History)
User_History.belongsTo(Languages)

User_History.hasMany(Users)
Users.belongsTo(User_History)

Levels.hasMany(User_History)
User_History.belongsTo(Levels)

Levels.hasMany(Words)
Words.belongsTo(Levels)

Languages.hasMany(Words)
Words.belongsTo(Languages)

TurkmenWords.hasMany(Words)
Words.belongsTo(TurkmenWords)

Languages.hasMany(Notifications)
Notifications.belongsTo(Languages)

Levels.hasMany(Notifications)
Notifications.belongsTo(Levels)

Users.hasMany(Messages)
Messages.belongsTo(Users)

module.exports = {
    Users, Languages, Roles, Words, TurkmenWords, 
    Levels, Notifications, Messages, User_History, Games
}