require('dotenv').config()
const jwt = require("jsonwebtoken")

module.exports = async function (req, res, next) {
    try {
        if (!req.cookies.abc) { return next() }
        jwt.verify(req.cookies.abc,
                process.env.PRIVATE_KEY, (err, decoded) => {
            if (err)
                return next()
            if (decoded)
                return res.status(200).redirect('/main')
        })
    } catch (error) {
        throw error
    }
}