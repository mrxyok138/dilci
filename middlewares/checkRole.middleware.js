const jwt = require('jsonwebtoken')

module.exports = function (requiredRoles) {
    return function (req, res, next) {
        try {
            const token = req.cookies.abc
            if (!token) {
                return res.status(401).redirect('/login')
            }
            const decoded = jwt.verify(token, process.env.PRIVATE_KEY)
            const access = requiredRoles.includes(decoded.roles)
            if (!access) {
                return res.status(403).render('/404')
            }
            req.user = decoded
            next()
        } catch (error) {
            throw error
        }
    }
}