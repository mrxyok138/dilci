const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    try {
        const token = req.cookies.game
        if (!token) { return res.redirect('/main') }
        const decoded = jwt.verify(token, process.env.PRIVATE_KEY)
        req.game = decoded
        next()
    } catch (error) {
        throw error
    }
}