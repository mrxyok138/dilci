require('dotenv').config()
const bcrypt = require('bcrypt')
const { Op } = require('sequelize')
const { Sequelize } = require('../config/database')
const { Users, Languages, Levels, TurkmenWords, Words, User_History, Roles, Notifications, Messages, Games } = require('../config/models')
const jwt = require('jsonwebtoken')

const generateJwt = (id, roles) => {
    return jwt.sign({ id, roles }, process.env.PRIVATE_KEY, { expiresIn: '30d' })
}

class AdminController {
    // GET
    async GetHome(req, res) {
        try {

            const users = await
                Users.findAll({
                    where: {
                        roleId: 3
                    },
                    attributes: ['id', 'image', 'username', 'email', 'phone', 'isActive'],
                    order: [['createdAt', 'ASC']]
                })

            const lang_words = await
                Words.findAll({
                    attributes: [
                        [Sequelize.literal('language.name'), 'language_name'],
                        [Sequelize.fn('count', Sequelize.col('word')), 'word_count']
                    ],
                    include: [
                        {
                            model: Languages,
                            attributes: []
                        }
                    ],
                    group: ['language_name']
                })

            const lang_count = await Languages.count()
            const word_count = await Words.count()

            return res.status(200).render('admin.home.ejs', {
                users_count: users.length,
                lang_count: lang_count,
                word_count: word_count,
                users: users,
                lang_words: lang_words
            })
        } catch (error) {
            throw error
        }
    }

    async GetLanguages(req, res) {
        try {

            const langs = await
                Languages.findAll({
                    attributes: ['id', 'name', 'image'],
                    order: [['id', 'ASC']]
                })

            const turkmen_words = await
                TurkmenWords.findAll({
                    attributes: ['id', 'word'],
                    order: [['id', 'ASC']]
                })

            const levels = await
                Levels.findAll({
                    attributes: ['id', 'name'],
                    order: [['id', 'ASC']]
                })

            const level_words = await
                Words.findAll({
                    attributes: [
                        [Sequelize.literal('level.id'), 'level_id'],
                        [Sequelize.literal('level.name'), 'level_name'],
                        [Sequelize.fn('count', Sequelize.col('word')), 'word_count']
                    ],
                    include: [
                        {
                            model: Levels,
                            attributes: []
                        }
                    ],
                    group: ['level_id', 'level_name'],
                    order: ['level_id']
                })

            const words = await
                Words.findAll({
                    attributes: ['id', 'word', 'point', 'transcription'],
                    include: [
                        {
                            model: TurkmenWords,
                            attributes: ['id', 'word']
                        },
                        {
                            model: Languages,
                            attributes: ['image']
                        },
                        {
                            model: Levels,
                            attributes: ['name']
                        }
                    ],
                    order: [['id', 'ASC']]
                })

            // return res.json({
            //     langs: langs,
            //     turkmen_words: turkmen_words,
            //     level_words: level_words,
            //     levels: levels,
            //     words: words,
            // })

            return res.status(200)
                .render('admin.langs.ejs', {
                    langs: langs,
                    turkmen_words: turkmen_words,
                    level_words: level_words,
                    levels: levels,
                    words: words
                })

        } catch (error) {
            throw error
        }
    }

    async GetUsers(req, res) {
        try {
            const usersHistories = await Users.findAll({
                where: { roleId: 3 },
                attributes: ['userHistoryId'],
                order: [['id', 'DESC']]
            })
            let userHistoryArr = []
            for (const item of usersHistories) {
                const userHistory = await User_History.findOne({
                    where: { id: item.userHistoryId },
                    attributes: ['id', 'point'],
                    include: [
                        {
                            model: Languages,
                            attributes: ['name']
                        },
                        {
                            model: Levels,
                            attributes: ['name']
                        },
                        {
                            model: Users,
                            attributes: ['id', 'image', 'username', 'isActive']
                        }
                    ]
                })
                userHistoryArr.push(userHistory)
            }
            return res.status(200).render('admin.users.ejs', { users: userHistoryArr })
        } catch (error) {
            throw error
        }
    }

    async GetNotifications(req, res) {
        try {
            const languages = await Languages.findAll({ attributes: ['id', 'name'] })
            const levels = await Levels.findAll({ attributes: ['id', 'name'] })
            return res.status(200).render('admin.addnotf.ejs', {
                languages: languages,
                levels: levels
            })
        } catch (error) {
            throw error
        }
    }

    async GetMessages(req, res) {
        try {
            const messages = await Messages.findAll({
                include: {
                    model: Users,
                    attributes: ['id', 'image', 'username', 'phone']
                }
            })
            return res.status(200).render('admin.messages.ejs', { messages: messages })
        } catch (error) {
            throw error
        }
    }

    async GetSearch(req, res) {
        try {
            return res.status(200).render('admin.search.ejs')
        } catch (error) {
            throw error
        }
    }

    async GetSettings(req, res) {
        try {
            return res.status(200).render('admin.settings.ejs')
        } catch (error) {
            throw error
        }
    }

    // POST

    async AddLanguagePost(req, res) {
        try {
            const { name } = req.body
            const image = req.file.filename
            if (name && image) {
                await Languages.create({
                    name: name,
                    image: image
                })
            }
            return res.status(201).redirect('/admin/langs')
        } catch (error) {
            throw error
        }
    }

    async AddTurkmenWordPost(req, res) {
        try {
            const { word } = req.body
            if (word) { await TurkmenWords.create({ word: word }) }
            return res.status(201).redirect('/admin/langs')
        } catch (error) {
            throw error
        }
    }

    async AddLanguageWordsPost(req, res) {
        try {
            const { word, ball, transcription, terjime, language, level } = req.body
            if (word && ball && transcription && terjime && language && level) {
                await Words.create({
                    word: word,
                    point: ball,
                    transcription: transcription,
                    turkmenWordId: terjime,
                    languageId: language,
                    levelId: level
                })
            }
            return res.status(201).redirect('/admin/langs')
        } catch (error) {
            throw error
        }
    }

    async AddAdminPost(req, res) {
        try {
            const { username, password, phone, email } = req.body
            if (username && password && email) {
                const user = await Users.findAll({
                    where: {
                        [Op.or]: {
                            username: username,
                            phone: phone,
                            email: email
                        }
                    }
                })
                if (user.length > 0) {
                    return res.redirect('/admin/settings')
                }
                const hash = bcrypt.hash(password, 5)
                const _user = await Users.create({
                    username: username,
                    password: hash,
                    phone: phone,
                    email: email
                })
                res.cookie('abc', generateJwt(_user.id, 'superadmin'), { httpOnly: true })
            } 
            return res.status(201).redirect('/admin/settings')
        } catch (error) {
            throw error
        }
    }

    async PostNotifications(req, res) {
        try {
            let { languageId, levelId, message } = req.body
            
            if (!message) { return res.status(401).redirect('/admin/notifications') }
            if (languageId === "null") { languageId = null }
            if (levelId === "null") { levelId = null }

            await Notifications.create({
                languageId: languageId,
                levelId: levelId,
                message: message
            }).then(() => { return res.status(201).redirect('/admin/notifications') })
            .catch((err) => {
                console.log(err)
                return res.redirect(401).redirect('/admin/notifications')
            })
        } catch (error) {
            throw error
        }
    }

    // UPDATE

    async UpdateAdminPost(req, res) {
        try {

            const { oldUsername, newUsername, password, phone, email } = req.body



        } catch (error) {
            throw error
        }
    }

    // DELETE

    async DeleteLanguage(req, res) {
        try {
            const { id } = req.params
            if (id) {
                await Languages.destroy({
                    where: {
                        id: id
                    }
                })
            }
            return res.status(200).redirect('/admin/langs')
        } catch (error) {
            throw error
        }
    }

    async DeleteTurkmenWords(req, res) {
        try {
            const { id } = req.params
            if (id) {
                await TurkmenWords.destroy({
                    where: {
                        id: id
                    }
                })
            }
            return res.status(200).redirect('/admin/langs')
        } catch (error) {
            throw error
        }
    }

    async DeleteWords(req, res) {
        try {
            const { id } = req.params
            if (id) {
                await Words.destroy({
                    where: {
                        id: id
                    }
                })
            }
            return res.status(200).redirect('/admin/langs')
        } catch (error) {
            throw error
        }
    }

    async DeleteUsers(req, res) {
        try {
            const { id } = req.params
            if (id) {
                await Games.destroy({
                    where: {
                        userId: id
                    }
                }).then(() => { console.log('user games deleted...') })
                .catch((err) => { console.log(err) })
                
                await User_History.destroy({
                    where: {
                        userId: id
                    }
                }).then(() => { console.log('user histories deleted...') })
                .catch((err) => { console.log(err) })
                
                await Messages.destroy({
                    where: {
                        userId: id
                    }
                }).then(() => { console.log('user messages deleted...') })
                .catch((err) => { console.log(err) })

                await Users.destroy({
                    where: {
                        id: id
                    }
                }).then(() => { console.log('user deleted...') })
                .catch((err) => { console.log(err) })
            }
            return res.status(200).redirect('/admin/users')
        } catch (error) {
            throw error
        }
    }

    async DeleteContact(req, res) {
        try {
            const { id } = req.params
            if (id) {
                await Messages.destroy({
                    where: {
                        id: id
                    }
                })
            }
            return res.status(200).redirect('/admin/messages')
        } catch (error) {
            throw error
        }
    }

    async Default(req, res) {
        try {
            
            await Roles.bulkCreate([
                { name: 'superadmin' },
                { name: 'admin' },
                { name: 'user' }
            ]).then(() => { console.log('Roles created') }).catch((err) => { console.log(err) })

            await Languages.bulkCreate([
                { name: 'Inlis dili', image: '1690818037659-english.png' },
                { name: 'Rus dili', image: '1690881926606-russian.png' }
            ]).then(() => { console.log('Languages created') }).catch((err) => { console.log(err) })

            await Levels.bulkCreate([
                { name: 'Beginner', point: 100 },
                { name: 'Elementary', point: 500 }
            ]).then(() => { console.log('Levels created') }).catch((err) => { console.log(err) })

            await TurkmenWords.bulkCreate([
                { word: 'Salam' },
                { word: 'Eje' },
                { word: 'Kaka' },
                { word: 'Mama' },
                { word: 'Baba' },
                { word: 'Dogan' },
                { word: 'Alma' },
                { word: 'Banan' }
            ]).then(() => { console.log('TurkmenWords created') }).catch((err) => { console.log(err) })

            await Words.bulkCreate([
                { word: 'Hello', transcription: '[Həˈlō]', levelId: 1, languageId: 1, turkmenWordId: 1, point: 2 },
                { word: 'Привет', transcription: '[Privet]', levelId: 1, languageId: 2, turkmenWordId: 1, point: 2 },
                { word: 'Mother', transcription: '[ˈMəT͟Hər]', levelId: 1, languageId: 1, turkmenWordId: 2, point: 2 },
                { word: 'Мама', transcription: '[Mama]', levelId: 1, languageId: 2, turkmenWordId: 2, point: 2 },
                { word: 'Father', transcription: '[ˈFäT͟Hər]', levelId: 1, languageId: 1, turkmenWordId: 3, point: 2 },
                { word: 'Отец', transcription: '[Otets]', levelId: 1, languageId: 2, turkmenWordId: 3, point: 2 },
                { word: 'Grandmother', transcription: '[Gran(d)ˌməT͟Hər]', levelId: 1, languageId: 1, turkmenWordId: 4, point: 3 },
                { word: 'Grandfather', transcription: '[Gran(d)ˌfäT͟Hər]', levelId: 1, languageId: 1, turkmenWordId: 5, point: 3 },
                { word: 'Brother', transcription: '[BrəT͟Hər]', levelId: 1, languageId: 1, turkmenWordId: 6, point: 3 },
                { word: 'Apple', transcription: '[Apəl]', levelId: 1, languageId: 1, turkmenWordId: 7, point: 3 },
                { word: 'Banana', transcription: '[Bəˈnanə]', levelId: 1, languageId: 1, turkmenWordId: 8, point: 3 }
            ]).then(() => { console.log('Words created') }).catch((err) => { console.log(err) })

            return res.status(200).redirect('/login')

        } catch (error) {
            throw error
        }
    }

}

module.exports = new AdminController()