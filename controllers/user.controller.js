require('dotenv').config()
const jwt = require('jsonwebtoken')
const { Op } = require('sequelize')
const { Sequelize } = require('../config/database')
const { Users, Languages, User_History, Levels, TurkmenWords, Words, Games, Notifications, Messages } = require('../config/models')

const dataToken = (gameId, sectionId, words) => {
    return jwt.sign({ gameId, sectionId, words }, process.env.PRIVATE_KEY, { expiresIn: '2m' })
}

class UserController {

    async HomeUser(req, res) {
        try {
            return res.status(200).render('index')
        } catch (error) {
            throw error
        }
    }

    async MainUser(req, res) {
        try {
            // Ulanyjyny alyar...
            const user = await Users.findOne({
                attributes: ['id', 'image', 'userHistoryId'],
                where: {
                    id: Number(req.user.id)
                }
            })
            // Eger ulanyjy yok bolsa...
            if (!user) {
                res.clearCookie('abc')
                return res.status(200).redirect('/login')
            }
            
            // Ulanyjynyn balyny alyar...
            const _user = await User_History.findOne({
                attributes: ['languageId', 'levelId'],
                where: {
                    id: user.userHistoryId
                }
            })
            const user_point = await User_History.findOne({
                attributes: ['point'],
                where: {
                    userId: user.id,
                    languageId: _user.languageId,
                    levelId: _user.levelId
                }
            })

            // Levelin baly...
            const level = await Levels.findOne({
                attributes: ['id', 'point'],
                where: {
                    id: _user.levelId
                }
            })

            // Eger ulanyjynyn baly Levelin balyndan gecse dejesi galyar...
            if (user_point.point > level.point) {
                await User_History.destroy({ where: { userId: user.id, languageId: _user.languageId, levelId: _user.levelId } })
                    .then(() => { console.log('User history pozuldy...') })
                    .catch((err) => { console.log(err) })
                const user_history = await User_History.create({
                    point: 0,
                    userId: user.id,
                    languageId: _user.languageId,
                    levelId: Number(_user.levelId) + 1
                })
                await Users.update({ userHistoryId: user_history.id }, { where: { id: user.id } })
                    .then(() => { console.log('User history update edildi...') })
                    .catch((err) => { console.log(err) })
            }

            return res.status(200).render('main', { user: user })

        } catch (error) {
            throw error
        }
    }

    async SettingsUser(req, res) {
        try {
            return res.status(200).render('settings')
        } catch (error) {
            throw error
        }
    }

    async NotificationsUser(req, res) {
        try {
            // Ulanyjynyn aktiw owrenyan dili we derejesini alyar...
            const user = await Users.findOne({
                attributes: ['userHistoryId'],
                where: {
                    id: Number(req.user.id)
                }
            })
            const _user = await User_History.findOne({
                attributes: ['languageId', 'levelId'],
                where: {
                    id: user.userHistoryId
                }
            })

            // Bildirishleri alyar...
            const notifications = await Notifications.findAll({
                attributes: ['id', 'message'],
                where: {
                    [Op.or]: [
                        { languageId: _user.languageId, levelId: _user.levelId },
                        { languageId: null, levelId: _user.levelId },
                        { languageId: _user.languageId, levelId: null },
                        { languageId: null, levelId: null }
                    ]
                }
            })

            // Islenen maglumatlary views/notifications.ejs shablonuna ugradyar...
            return res.status(200).render('notifications', { notifications })

        } catch (error) {
            throw error
        }
    }

    async ProfileUser(req, res) {
        try {

            const user = await Users.findOne({
                attributes: ['id', 'username', 'image', 'userHistoryId'],
                where: {
                    id: req.user.id
                },
                include: {
                    model: User_History,
                    attributes: ['languageId'],
                    include: {
                        model: Languages,
                        attributes: ['name']
                    }
                }
            })

            const userPoint = await User_History.findOne({
                attributes: ['point', 'levelId', 'languageId'],
                where: {
                    id: user.userHistoryId,
                },
                include: [
                    {
                        model: Levels,
                        attributes: ['name']
                    },
                    {
                        model: Languages,
                        attributes: ['name']
                    }
                ]
            })

            const userRating = await User_History.findAll({
                where: {
                    languageId: userPoint.languageId,
                    levelId: userPoint.levelId
                },
                attributes: ['userId', 'point'],
                order: [['point', 'DESC']]
            })

            const rating = userRating.findIndex(item => {
                return item.userId === user.id
            }) + 1

            const levels = await Levels.findAll({ attributes: ['name', 'point'] })

            return res.status(200).render('profile', {
                user: user,
                levels: levels,
                rating: rating,
                userPoint: userPoint
            })

        } catch (error) {
            throw error
        }
    }

    async LanguagesUser(req, res) {
        try {

            const langs = await
                Languages.findAll({
                    attributes: ['id', 'name', 'image']
                })

            return res.status(200).render('language', { langs: langs })

        } catch (error) {
            throw error
        }
    }

    async ToBuyUser(req, res) {
        try {
            return res.status(200).render('tobuy')
        } catch (error) {
            throw error
        }
    }

    async ContactUser(req, res) {
        try {
            return res.status(200).render('contact')
        } catch (error) {
            throw error
        }
    }

    async LogoutUser(req, res) {
        try {
            res.clearCookie('abc')
            return res.status(200).redirect('/login')
        } catch (error) {
            throw error
        }
    }

    async RatingUser(req, res) {
        try {

            const user = await Users.findOne({
                attributes: ['userHistoryId'],
                where: {
                    id: req.user.id
                }
            })

            const userRatings = await User_History.findOne({
                attributes: ['languageId', 'levelId'],
                where: {
                    id: user.userHistoryId
                }
            })

            const _userRatings = await User_History.findAll({
                attributes: ['userId', 'point'],
                where: {
                    languageId: userRatings.languageId,
                    levelId: userRatings.levelId
                },
                include: {
                    model: Users,
                    attributes: ['id', 'image', 'username']
                },
                order: [['point', 'DESC']]
            })

            return res.status(200).render('ratings', {
                ratings: _userRatings
            })

        } catch (error) {
            throw error
        }
    }

    async ToCompete(req, res) {
        try {
            // id - Adata oyun ya-da Basdesik oyundygyny anladyar
            const { id } = req.params
            const sectionId = Date.now()

            // Eger oyun id 1 ya-da 2 bolmasa...
            if (Number(id) !== 1 && Number(id) !== 2) { return res.status(404).render('404') }

            // Ulanyjynyn haysy dilde oyuna bashlamalydygyny hasaplayar... 
            const userHistoryId = await Users.findOne({
                attributes: ['userHistoryId'],
                where: {
                    id: Number(req.user.id)
                }
            })
            const user = await User_History.findOne({
                attributes: ['languageId', 'levelId'],
                where: {
                    id: userHistoryId.userHistoryId
                }
            })

            // Dile we Dereja gora 10 sany random sozleri getiryar...
            const words = await Words.findAll({
                attributes: ['id', 'word', 'transcription'],
                where: {
                    languageId: user.languageId,
                    levelId: user.levelId
                },
                include: {
                    model: TurkmenWords,
                    attributes: ['id', 'word']
                },
                order: Sequelize.literal('RANDOM()'),
                limit: 10
            })

            // Alnan sozleri browserde yatda saklayar
            res.cookie('game', dataToken(id, sectionId, words), { httpOnly: true })

            // Islenen maglumatlary views/words.ejs shablonuna ugradyar...
            return res.status(200).render('words', { words: words })

        } catch (error) {
            throw error
        }
    }

    async Game(req, res) {
        try {
            // game - oyun maglumatlary, user - ulanyjy maglumatlary
            const { game, user } = req
            const page = req.query.page

            // Eger maglumatlardan biri yok bolsa, main sahypasyna ugradyar...
            if (!game || !user || !page || page < 0) { return res.status(401).redirect('/main') }

            //  Eger sahypa gutaran bolsa jemleyar...
            if (page > game.words.length) {

                // Usere degisli netijeleri database'den alyar... 
                let results = await Games.findAll({
                    attributes: ['id', 'status', 'sectionId'],
                    where: {
                        userId: user.id,
                        sectionId: game.sectionId
                    },
                    include: [
                        {
                            model: Users,
                            attributes: ['username']
                        },
                        {
                            model: Words,
                            attributes: ['point'],
                        }
                    ],
                    order: [['id', 'ASC']]
                })

                // Dogry jogaplaryn ballaryny jemleyar...
                let point = 0
                results.forEach((item) => {
                    if (item.status === 'true' && item.word) {
                        point += item.word.point
                    }
                })

                // Ulanyjydaky on bar bolan baly alyar...
                const userHistoryId = await Users.findOne({
                    attributes: ['userHistoryId'],
                    where: {
                        id: Number(user.id)
                    }
                })
                const _user = await User_History.findOne({
                    attributes: ['languageId', 'levelId'],
                    where: {
                        id: userHistoryId.userHistoryId
                    }
                })
                const user_point = await User_History.findOne({
                    attributes: ['point'],
                    where: {
                        userId: user.id,
                        languageId: _user.languageId,
                        levelId: _user.levelId
                    }
                })

                // Ulanyjydaky baly taze alan baly bilen jemlap update edyar... 
                const sum = user_point.point ? user_point.point + point : point
                await User_History.update(
                    { point: sum },
                    {
                        where: {
                            userId: user.id,
                            languageId: _user.languageId,
                            levelId: _user.levelId
                        }
                    })
                    .then(() => { console.log('Ball goshuldy...') })
                    .catch((err) => { console.log(err) })

                // Islenen maglumatlary views/result.ejs shablonuna ugradyar...
                return res.status(200).render('result', { results: results, point: point })
            }

            // Turkmen sozi we jogabyny oz icine alyan ululyklar
            let i = 0
            let len = 5
            let index = page - 1
            let random_items = []
            let correct_answer = game.words[index]
            let turkmen_word = game.words[index].turkmen_word

            // Eger sozler 5 den kici bolsa 2 sany random soz almaly
            if (game.words.length < 5) { len = 2 }

            // Random jogaplary hodurleyar...
            while (i < len) {

                // Random jogaplary saylayar...
                const random_index = Math.floor(Math.random() * game.words.length)
                const random_item = game.words.splice(random_index, 1)[0]

                // Eger random item dogry jogap bolsa, jogaby push etdirenok...
                if (correct_answer.id == random_item.id) { continue }

                // Jogaby push edyar...
                random_items.push(random_item)
                i += 1
            }

            // Sonunda dogry jogaby random index'e salyar...
            const random_index = Math.floor(Math.random() * random_items.length)
            random_items.splice(random_index, 1, correct_answer)

            // Eger Adaty oyun bolsa...
            if (Number(game.gameId) === 1) {
                return res.status(200).render('game', {
                    turkmen_word: turkmen_word,
                    random_items: random_items,
                    page: page
                })
            }

            // Eger Bashdeshlik oyun bolsa...
            if (Number(game.gameId) === 2) {
                
            }

        } catch (error) {
            throw error
        }
    }

    async Reply(req, res) {
        try {
            // answer - ulanyjynyn beren jogaby, result - dogry jogabyn id'si...
            const { answer, result, page } = req.query
            const { game, user } = req

            // Eger ulanyjy jogap bermedik bolsa...
            if (Number(answer) === -1) {
                await Games.create({
                    gameId: game.gameId,
                    sectionId: game.sectionId,
                    status: 'unanswered',
                    userId: user.id,
                    wordId: null
                })
            }

            // Eger ulanyjy jogap beren bolsa...
            game.words.forEach(async (item) => {
                if (item.turkmen_word.id == result) {
                    // Dogry jogap berse...
                    if (item.id == answer) {
                        await Games.create({
                            gameId: game.gameId,
                            sectionId: game.sectionId,
                            status: 'true',
                            userId: user.id,
                            wordId: item.id
                        })
                    } else {
                        // Yalnysh jogap berse...
                        await Games.create({
                            gameId: game.gameId,
                            sectionId: game.sectionId,
                            status: 'false',
                            userId: user.id,
                            wordId: item.id
                        })
                    }
                }
            })

            // Ulanyjyny indiki sahypa ugradyar...
            return res.redirect(`/game/begin/q?page=${Number(page) + 1}`)

        } catch (error) {
            throw error
        }
    }

    // POST

    async UserOffline(req, res) {
        try {
            const { id } = req.body

            console.log(id);

        } catch (error) {
            throw error
        }
    }

    async ContactUserPost(req, res) {
        try {
            const { username, email, text } = req.body
            const { user } = req

            if (username && email && text) {
                await Messages.create({
                    username: username,
                    email: email,
                    message: text,
                    userId: user.id
                })
            }
            return res.status(201).redirect('/contact')

        } catch (error) {
            throw error
        }
    }

    // UPDATE

    async LanguagesUserUpdate(req, res) {
        try {
            const { lang } = req.params
            const [user, created] = await User_History.findOrCreate({
                where: {
                    userId: req.user.id,
                    languageId: Number(lang)
                },
                defaults: {
                    point: 0,
                    userId: req.user.id,
                    languageId: Number(lang),
                    levelId: 1
                }
            })

            if (user && created === false) {
                await Users.update(
                    { userHistoryId: user.id }, {
                    where: {
                        id: req.user.id
                    }
                })
            }

            return res.status(201).redirect('/user')

        } catch (error) {
            throw error
        }
    }

    async LanguageUserChange(req, res) {
        try {
            const { id } = req.params

            const _id = await User_History.findOne({
                attributes: ['id'],
                where: {
                    userId: req.user.id,
                    languageId: id
                }
            })

            await Users.update(
                { userHistoryId: _id.id },
                {
                    where: {
                        id: req.user.id
                    }
                }
            )

            return res.status(201).redirect('/user')

        } catch (error) {
            throw error
        }
    }

    async EditProfile(req, res) {
        try {
            let obj = {}
            const userId = Number(req.user.id)
            for (let item in req.body) {
                if (req.body[item].length > 0) {
                    obj[item] = req.body[item]
                }
            }
            if (req.file) { obj['image'] = req.file.filename }

            const exists = await Users.findAll({
                attributes: ['id'],
                where: {
                    [Op.or]: obj
                }
            })
            if (exists.length > 0) {
                console.log(false)
                return res.status(201).redirect('/user')
            }

            await Users.update(obj, { where: { id: Number(userId) }})
                .then(() => { console.log(true) })
                .catch((err) => { console.log(err) })

            return res.status(201).redirect('/user')

        } catch (error) {
            throw error
        }
    }

}

module.exports = new UserController()