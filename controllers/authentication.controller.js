const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const { Op } = require('sequelize')
const { Users, Roles, User_History } = require('../config/models')

const generateJwt = (id, roles) => {
    return jwt.sign({ id, roles }, process.env.PRIVATE_KEY, { expiresIn: '30d' })
}

class AuthenticationController {

    async UserLoginGet(req, res) {
        try {
            return res.status(200).render('login')
        } catch (error) {
            throw error
        }
    }

    async UserRegisterGet(req, res) {
        try {
            return res.status(200).render('register')
        } catch (error) {
            throw error
        }
    }

    async UserLoginPost(req, res) {
        try {
            const { username, password } = req.body
            const user = await Users.findOne({
                attributes: ['id', 'image', 'password'],
                where: { username: username },
                include: [
                    {
                        model: Roles,
                        attributes: ['name']
                    }
                ]
            })
            if (!user) { return res.status(401).redirect('/login') }
            const hashed = await bcrypt.compare(password, user.password)
            if (hashed === true) {
                res.cookie('abc', generateJwt(user.id, user.role.name), { httpOnly: true })
                return res.status(200).redirect('/main')
            }
            return res.status(401).redirect('/login')
        } catch (error) {
            throw error
        }
    }

    async UserRegisterPost(req, res) {
        try {
            const { username, email, phone, password } = req.body
            const user = await Users.findAll({
                where: {
                    [Op.or]: {
                        username: username,
                        email: email,
                        phone: phone
                    }
                }
            })
            if (user.length > 0) { return res.status(503).redirect('/register') }
            const hashed = await bcrypt.hash(password, 5)

            if (username && password && email && phone) {
                const _user = await Users.create({
                    username: username,
                    password: hashed,
                    email: email,
                    phone: phone,
                    roleId: 3
                })
                const user_history = await
                    User_History.create({
                        point: 0,
                        userId: _user.id,
                        languageId: 1,
                        levelId: 1
                    })
                _user.userHistoryId = user_history.id
                await _user.save()
                res.cookie('abc', generateJwt(_user.id, 'user'), { httpOnly: true })
                return res.status(200).render('main', { user: _user })
            }
            return res.status(401).redirect('/register')
            
        } catch (error) {
            throw error
        }
    }

}

module.exports = new AuthenticationController()