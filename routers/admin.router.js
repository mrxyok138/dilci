require('dotenv').config()
const router = require('express').Router()
const upload = require('../middlewares/imageUpload.middleware')
const { 
    GetHome, 
    AddLanguagePost, 
    GetLanguages,
    GetNotifications,
    PostNotifications,
    GetUsers,
    GetMessages,
    GetSearch,
    GetSettings,
    AddAdminPost,
    UpdateAdminPost,
    DeleteLanguage,
    AddTurkmenWordPost,
    DeleteTurkmenWords,
    AddLanguageWordsPost,
    DeleteWords,
    DeleteUsers,
    DeleteContact,
    Default
} = require('../controllers/admin.controller')

// admin.home
router.route('/').get(GetHome)
// admin.langs
router.route('/langs').get(GetLanguages).post(upload(process.env.IMG_PATH).single('image'), AddLanguagePost)
router.route('/langs/turkmen/words').post(AddTurkmenWordPost)
router.route('/langs/add/words').post(AddLanguageWordsPost)
// admin.users
router.route('/users').get(GetUsers)
router.route('/notifications').get(GetNotifications).post(PostNotifications)
router.route('/messages').get(GetMessages)
router.route('/search').get(GetSearch)
router.route('/settings').get(GetSettings)
router.route('/add/admin').post(AddAdminPost)
router.route('/update/admin').post(UpdateAdminPost)

// DELETE

router.route('/delete/lang/:id').get(DeleteLanguage)
router.route('/delete/turkmen_word/:id').get(DeleteTurkmenWords)
router.route('/delete/word/:id').get(DeleteWords)
router.route('/delete/user/:id').get(DeleteUsers)
router.route('/delete/contact/:id').get(DeleteContact)

// DEFAULT

router.route('/default').get(Default)

module.exports = router