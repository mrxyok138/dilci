const router = require('express').Router()
const {
    HomeUser,
    MainUser,
    SettingsUser,
    NotificationsUser,
    ProfileUser,
    LanguagesUser,
    ToBuyUser,
    ContactUser,
    ContactUserPost,
    LanguagesUserUpdate,
    LogoutUser,
    LanguageUserChange,
    RatingUser,
    UserOffline,
    ToCompete,
    Game,
    Reply,
    EditProfile
} = require('../controllers/user.controller')
const {
    UserLoginGet,
    UserRegisterGet,
    UserLoginPost,
    UserRegisterPost
} = require('../controllers/authentication.controller')
const auth = require('../middlewares/auth.middleware')
const checkRole = require('../middlewares/checkRole.middleware')
const gameMiddleware = require('../middlewares/game.middleware')
const imageMiddleware = require('../middlewares/imageUpload.middleware')

router.route('/').get(auth, HomeUser)
router.route('/login').get(auth, UserLoginGet).post(auth, UserLoginPost)
router.route('/register').get(auth, UserRegisterGet).post(auth, UserRegisterPost)
router.route('/main').get(checkRole(['user', 'admin']), MainUser)
router.route('/settings').get(checkRole(['user', 'admin']), SettingsUser)
router.route('/notifications').get(checkRole(['user', 'admin']), NotificationsUser)
router.route('/user').get(checkRole(['user', 'admin']), ProfileUser)
router.route('/langs').get(checkRole(['user']), LanguagesUser)
router.route('/langs/:lang').get(checkRole(['user']), LanguagesUserUpdate)
router.route('/tobuy').get(checkRole(['user']), ToBuyUser)
router.route('/contact').get(checkRole(['user', 'admin']), ContactUser).post(checkRole(['user']), ContactUserPost)
router.route('/logout').get(checkRole(['user']), LogoutUser)
router.route('/lang/change/:id').get(checkRole(['user']), LanguageUserChange)
router.route('/ratings').get(checkRole(['user', 'admin']), RatingUser)
router.route('/game/:id').get(checkRole(['user']), ToCompete)
router.route('/game/begin/q').get(gameMiddleware, checkRole(['user']), Game)
router.route('/game/reply/q').get(gameMiddleware, checkRole(['user']), Reply)
router.route('/edit/profile').post(imageMiddleware(process.env.IMAGE_PATH).single('image'), checkRole(['user', 'admin']), EditProfile)

router.route('/user/off').post(UserOffline)

module.exports = router