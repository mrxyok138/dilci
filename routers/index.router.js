const router = require('express').Router()
const userRouter = require('./user.router')
const adminRouter = require('./admin.router')

router.use('/', userRouter)
router.use('/admin', adminRouter)

module.exports = router