const btn = document.querySelectorAll('.admin__button')
const bottom = document.querySelectorAll('.admin__bottom')

for (let i = 0; i < btn.length; i++) {
    btn[i].addEventListener('click', () => {
        if (bottom[i].style.display == 'none') {
            btn[i].style.rotate = '180deg'
            bottom[i].style.display = 'block'
        } else {
            btn[i].style.rotate = '0deg'
            bottom[i].style.display = 'none'
        }
    }, false)
}


const profile_lang = document.querySelectorAll('.profile__lang')

for (let i = 0; i < profile_lang.length; i++) {
    profile_lang[i].addEventListener('click', () => {
        document.querySelector('.active').classList.remove('active')
        profile_lang[i].classList.add('active')
    }, false)
}

const profile__edit = document.querySelector('.profile__edit')
const closed = document.querySelector('.modal__close')

profile__edit.addEventListener('click', () => {
    document.querySelector('.profile__modal').style.display='block'
}, false)
closed.addEventListener('click', () => {
    document.querySelector('.profile__modal').style.display='none'
}, false)