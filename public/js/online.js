window.addEventListener("unload", async (e) => {
    const user = await fetch('/user/get', { method: 'GET', headers: {
        'Content-Type': 'application/json'
    } })
                .then(res => res.json())
                .then(data => console.log(data))
    console.log(user);
    
    fetch('/user/off', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            id: 5
        })
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('HTTP error, status: ', response.status)
        }
    })
    .catch(error => {
        console.log('Error ', error);
    })
})