let game_time = document.getElementById('time')
game_time.innerHTML = `0:${localStorage.getItem('time')}`
const interval = setInterval(() => {
    localStorage.setItem('time', Number(localStorage.getItem('time')) - 1)
    if (Number(localStorage.getItem('time')) <= 0) {
        alert('Wagtynyz doldy!')
        clearInterval(interval)
    }
    game_time.innerHTML = `0:${localStorage.getItem('time')}`
}, 1000)